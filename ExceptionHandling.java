import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            System.out.print("Masukkan nilai pembilang: ");
            int pembilang = scanner.nextInt();
            try{
                System.out.print("Masukkan nilai penyebut: ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil pembagian adalah "+ hasil);
                validInput = true;
            } catch (InputMismatchException e ){
                System.err.println("Error: Penyebut harus berisi bilangan bulat");
                scanner.nextLine(); //untuk membersihkan buffer masukan setelah terjadi InputMismatchException
            } catch (NullPointerException e){
                System.err.println("Error: Penyebut tidak boleh 0");
                scanner.nextLine(); //untuk membersihkan buffer masukan setelah terjadi NullPointException
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0){
            throw new NullPointerException("Error: Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut;
    }
}
